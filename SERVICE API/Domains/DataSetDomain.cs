using System.ComponentModel.DataAnnotations;
using MassTransit;
using message;

public class DataSetDomain{
    [Key]
    public int IdMessage {get; set;}
    public string Message {get; set;}
    public int? IdReference {get; set;}
    public DateTime? ReferenceDateTime {get; set;}
    public DateTime CreateDateTime {get; set;}
    public DateTime? UpdateDateTime {get; set;}
    public DataSetDomain()
    {
        CreateDateTime = DateTime.Now; // UtcNow;
    }
    public DataSetDomain AddMessageRecived(string message)
    {
        Message = message;
        return this;
    }

    public DataSetDomain UpdateMessageRecived(int idReference, DateTime referenceDateTime) 
    {   
        IdReference = idReference;
        ReferenceDateTime =  referenceDateTime;
        UpdateDateTime = DateTime.Now;
        return this;
    }
}
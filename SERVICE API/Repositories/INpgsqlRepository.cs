
public interface INpgsqlRepository: IDisposable
{
    bool AddNewDataSet(DataSetDomain message);
    bool UpdateDataSet(DataSetDomain message);
}
public interface ISqlLiteRepository : IDisposable
{
    bool AddNewDataSet(DataSetDomain message);
    bool UpdateDataSet(DataSetDomain message);
}
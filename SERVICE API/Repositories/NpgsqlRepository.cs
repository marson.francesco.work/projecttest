
public class NpgsqlRepository : INpgsqlRepository
{

    private readonly NpgsqlContext _context;
    public NpgsqlRepository(NpgsqlContext context)
    {
        _context = context;
    }


    public bool AddNewDataSet(DataSetDomain message)
    {
        try
        {
        _context.DataSet.Add(message);
        _context.SaveChanges();
        return true;
        }
        catch(Exception e)
        {
            throw new Exception(e.ToString());
        }
    }

    public bool UpdateDataSet(DataSetDomain message)
    {
        try
        {
            _context.DataSet.Update(message);
            _context.SaveChanges();
            return true;
        }
        catch(Exception e)
        {
            throw new Exception(e.ToString());
        }
    }

    public void Dispose()
    {
        throw new NotImplementedException();
    }

}
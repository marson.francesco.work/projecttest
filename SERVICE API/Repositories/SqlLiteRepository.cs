
public class SqlLiteRepository : ISqlLiteRepository, IDisposable
{
    private readonly SqlLiteContext _context;
    public SqlLiteRepository(SqlLiteContext context)
    {
        _context = context;
    }
    public bool AddNewDataSet(DataSetDomain data)
    {
        try
        {
            _context.DataSet.Add(data);
            _context.SaveChanges();
            return true;
        }
        catch(Exception e)
        {
            throw new Exception(e.ToString());            
        }
    }

    public bool UpdateDataSet(DataSetDomain data)
    {
        try
        {
            _context.DataSet.Update(data); 
            _context.SaveChanges(); 
            return true;
        } 
        catch(Exception e) 
        {
            throw new Exception(e.ToString());
        }
    }

    public void Dispose() {}
}

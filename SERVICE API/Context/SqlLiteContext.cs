using Microsoft.EntityFrameworkCore;

public class SqlLiteContext: DbContext
{
    public DbSet<DataSetDomain> DataSet { get; set; }
    public SqlLiteContext(DbContextOptions options) : base() {}
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // options.UseSqlite("Data Source=./main_configuration.db"); 
        options.UseSqlite("Data Source=./DB/SqlLiteDB/main_configuration.db"); 
    }
}

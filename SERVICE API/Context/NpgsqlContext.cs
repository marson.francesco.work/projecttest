using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

public class NpgsqlContext : DbContext
{
    public DbSet<DataSetDomain> DataSet { get; set; }
    public NpgsqlContext(DbContextOptions options) : base() {}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       => optionsBuilder.UseNpgsql("Host=localhost;Database=augeg4;Username=postgres;Password=postgres");

}

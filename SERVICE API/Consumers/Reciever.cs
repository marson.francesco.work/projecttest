using System.Text;
using MassTransit;
using message;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using message;

public class RecivierAPI: IConsumer<DataSetToApi>
{
    private readonly SqlLiteContext _context;
    private IModel channel {get; set;}
    private IHandlerReceiver _handler;
    
    public RecivierAPI(IHandlerReceiver handler)
    {
        _handler = handler;
    }
    public async Task Consume(ConsumeContext<DataSetToApi> context)
    {   
        _handler.Handle(context);
    }
}

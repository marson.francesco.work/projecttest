
using message;

public interface IPublisher{
    public void Send(DataSetToSatellite message);
}

using System.Text;
using MassTransit;
using message;
using Newtonsoft.Json;

public class Publisher: IPublisher{

    readonly IBus _bus;
    public Publisher(IBus bus)
    {
        _bus = bus;
    }

    public async void Send(DataSetToSatellite message){

        await _bus.Publish<DataSetToSatellite>(message);

    }
}
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<IPublisher, Publisher>();

builder.Services.AddTransient<IHandelrProcess, HandlerProcess>();

builder.Services.AddTransient<IHandlerReceiver, HandlerReceiver>();

builder.Services.AddTransient<ISqlLiteRepository, SqlLiteRepository>();

builder.Services.AddDbContext<SqlLiteContext>();

builder.Services.AddDbContext<NpgsqlContext>();

// builder.Services.AddDbContext<MongoDbContext>();
// builder.Services.AddDbContext<SqlLiteContext>().As<BaseContext>();

builder.Host.ConfigureServices((hostContext, services) =>
        {
            services.AddMassTransit(x =>
            {
                services.AddMassTransit(x =>
                {
                    x.UsingRabbitMq((context,cfg) =>
                    {
                        cfg.Host("localhost", "/", h => {
                            h.Username("guest");
                            h.Password("guest");
                        });
                        cfg.ConfigureEndpoints(context);
                    });
                    x.AddConsumer<RecivierAPI>();
                });
            });
        });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.UseCors(options => options
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.MapControllers();

InitializeDatabase(app);

app.Run();

static void InitializeDatabase(IApplicationBuilder app)
{
    using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
    {
        var sqlLiteContext = serviceScope.ServiceProvider.GetRequiredService<SqlLiteContext>();
        if (sqlLiteContext.Database.EnsureCreated()) {}

        var postgreContext = serviceScope.ServiceProvider.GetRequiredService<NpgsqlContext>();
        if (postgreContext.Database.EnsureCreated()) {}
    }
}

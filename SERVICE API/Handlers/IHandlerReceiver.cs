using MassTransit;
using message;
public interface IHandlerReceiver
{
    public bool Handle(ConsumeContext<DataSetToApi> context);
}

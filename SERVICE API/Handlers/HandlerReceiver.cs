using MassTransit;
using message;
using Newtonsoft.Json;

public class HandlerReceiver: IHandlerReceiver
{
    private readonly SqlLiteContext _context;
    private readonly SqlLiteRepository _repository;

    public HandlerReceiver() {}

    public HandlerReceiver(SqlLiteContext context)
    {
        _context = context;
    }

    public bool Handle(ConsumeContext<DataSetToApi> context)
    {
        DataSetDomain data = _context.DataSet.FirstOrDefault(a => a.IdMessage == context.Message.idMessage);
        if(data != null)
        {
            data.UpdateMessageRecived(context.Message.idReference, context.Message.referenceDateTime);
            return true;
        } else 
        {
            return false;
        }
    }
}

using MassTransit;
using message;
using Newtonsoft.Json;

public class HandlerProcess: IHandelrProcess
{
    private readonly IPublisher _publisher;
    private readonly ISqlLiteRepository _sqlLiteRepository;
    public HandlerProcess(
        IPublisher publisher, 
        ISqlLiteRepository sqlLiteRepository)
    {
        // _context = context;
        _publisher = publisher;
        _sqlLiteRepository =  sqlLiteRepository;
    }
    public async Task<bool> Handle(DataSetMsg message)
    {
        // Creo l'oggetto che andrà salvato nel database
        DataSetDomain data = new DataSetDomain();
        data.AddMessageRecived(JsonConvert.SerializeObject(message));

        // chiamo il repository dove andrò a salvare l'elemento  

        

        _sqlLiteRepository.AddNewDataSet(data);

        // Prendo tutti i valori ricevuti e li somma
        var conteggio = 0;
        message.DataSetMsgItem.ForEach(a => {
            conteggio += a.value;
        });
        // Invio comando di BUS
        DataSetToSatellite dataSet = new DataSetToSatellite{
            IdMessage = data.IdMessage, // mi recupera l'id 
            Value = conteggio
        };
        _publisher.Send(dataSet);
        return true;
    }
}

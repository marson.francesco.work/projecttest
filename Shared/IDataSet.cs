namespace message
{
    public interface IDataSet
    {
        int IdMessage { get; set; }
        int Value { get; set; }
    }
    public class DataSetToSatellite : IDataSet
    {
        public int IdMessage { get; set; }
        public int Value { get; set; }
    }
    public class DataSetToApi // : IDataSet
    {
        // public int IdMessage { get; set; }
        // public int Value { get; set; }
        public int idReference {get; set;}
        public int idMessage  {get; set;}
        public DateTime referenceDateTime {get; set;}
    }

    public class ValueToSaveSatellite {
        public int idReference {get; set;}
        public int idMessage {get; set;}
        public int value {get; set;}
        public DateTime dateTime {get; set;}
    }
}

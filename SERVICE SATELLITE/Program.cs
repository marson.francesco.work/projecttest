using MassTransit;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        // Add services to the container.
        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        
        builder.Services.AddSwaggerGen();

        builder.Services.AddSingleton<IPublisher, Publisher>();

        builder.Services.AddDbContext<DataSetContext>();

        builder.Services.AddTransient<IHandelrProcess, HandlerProcess>();

        builder.Host.ConfigureServices((hostContext, services) =>
        {
            services.AddMassTransit(x =>
            {
                services.AddMassTransit(x =>
                {
                    x.UsingRabbitMq((context,cfg) =>
                    {
                        cfg.Host("localhost", "/", h => {
                            h.Username("guest");
                            h.Password("guest");
                        });
                        cfg.ConfigureEndpoints(context);
                    });
                    x.AddConsumer<RecivierSatellite>();
                });
            });
        });

        var app = builder.Build();
        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseAuthorization();

        app.UseCors(options => options
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

        app.MapControllers();

        InitializeDatabase(app);

        app.Run();

        static void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DataSetContext>();
                if (context.Database.EnsureCreated()) {}
            }
        }
    }
}
using System.ComponentModel.DataAnnotations;

public class DataSetSatelliteDomain {
    [Key]
    public int IdReference {get; set;}
    public int IdMessage {get; set;}
    public int Value {get; set;}
    public DateTime DateTime {get; set;}

    public DataSetSatelliteDomain()
    {
        DateTime = DateTime.Now; // UtcNow;
    }
    
    public DataSetSatelliteDomain(int myidMessage, int myvalue) 
    {
        IdMessage = myidMessage;
        Value = myvalue; 
        DateTime = DateTime.UtcNow;
    }
}
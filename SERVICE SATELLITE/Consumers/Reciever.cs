using System.Text;
using MassTransit;
using message;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

public class RecivierSatellite: IConsumer<DataSetToSatellite>{
    private IModel channel {get; set;}
    private IHandelrProcess _handler;
    
    public RecivierSatellite(IHandelrProcess handler)
    {
        _handler = handler;
    }
    public async Task Consume(ConsumeContext<DataSetToSatellite> context) 
    {
        _handler.Handle(context.Message);
    }
}
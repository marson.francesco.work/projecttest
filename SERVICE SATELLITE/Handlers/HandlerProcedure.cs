using message;
using Newtonsoft.Json;

public class HandlerProcess: IHandelrProcess
{
    private readonly DataSetContext _context;
    private readonly IPublisher _publisher;
    public HandlerProcess( IPublisher publisher, DataSetContext context )
    {
        _context = context;  
        _publisher = publisher;             
    }
    public bool Handle(DataSetToSatellite message)
    {
        // salvo i dati nel database
        DataSetSatelliteDomain data = new DataSetSatelliteDomain(message.IdMessage, message.Value);
        _context.DataSet.Add(data);
        _context.SaveChanges();

        // invio messaggio bus di risposta 
        DataSetToApi dataSet = new DataSetToApi{
            idReference = data.IdReference,
            idMessage = message.IdMessage,
            referenceDateTime = DateTime.UtcNow
        };

         _publisher.Send(dataSet);
        return true;
    }
}
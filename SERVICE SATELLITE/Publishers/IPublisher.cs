
using message;

public interface IPublisher{
    public void Send(DataSetToApi message);
}
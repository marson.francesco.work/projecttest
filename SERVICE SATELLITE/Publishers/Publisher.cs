using System.Text;
using MassTransit;
using message;
using Newtonsoft.Json;
using RabbitMQ.Client;

public class Publisher: IPublisher{

    readonly IBus _bus;
    public Publisher(IBus bus)
    {
        _bus = bus;
    }

    public async void Send(DataSetToApi message){

        await _bus.Publish<DataSetToApi>(message);

    }
}
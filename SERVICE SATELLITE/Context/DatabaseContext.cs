
using Microsoft.EntityFrameworkCore;

public class DataSetContext : DbContext{
    public DbSet<DataSetSatelliteDomain> DataSet { get; set; }
    public DataSetContext(DbContextOptions options) : base() { }
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options.UseSqlite("Data Source=./main_configuration.db");
    }
}

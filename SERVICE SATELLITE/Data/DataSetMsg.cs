
public class DataSetMsg{

    public List<DataSetMsgItem> DataSetMsgItem {get; set;}
    
    public DataSetMsg()
    {
        DataSetMsgItem = new List<DataSetMsgItem>();
    }
}

public class DataSetMsgItem{
    public int id {get; set;}
    public int value {get; set;}
}


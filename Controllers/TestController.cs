using Microsoft.AspNetCore.Mvc;

namespace SERVICE_API.Controllers;

[ApiController]
[Route("[controller]")]
public class APIController : ControllerBase
{

    private readonly ILogger<WeatherForecastController> _logger;
    private readonly IHandelrProcess _handler;

    public APIController(
        ILogger<WeatherForecastController> logger,
        IHandelrProcess handler
        )
    {
        _logger = logger;
        _handler = handler;
        
    }


    [HttpPost]
    public async Task<bool> Post([FromBody] DataSetMsg data)
    {
       
       return await _handler.Handle(data);
    }
}
